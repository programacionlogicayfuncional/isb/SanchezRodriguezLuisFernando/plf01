(ns plf01.core)
;Ejercicios 1 <
(defn función-<-1
  [ a b ]
  (< a b))

(defn función-<-2 
  [a b]
  (< (inc a) b))

(defn función-<-3
  [vector b]
  (< (count vector) b))

(función-<-1 2 4)
(función-<-2 2 2)
(función-<-3 [1 5 6 87 4] 2)

; Ejercicios 2 <=
(defn función-<=-4
  [a b c]
  (<= a b c))

(defn función-<=-5
  [a s]
  (if (neg? a) [* a -1] [* a 1])
  (if (neg? s) [* s -1] [* s 1])
   (<= a s))

(defn función-<=-6 
  [a b]
  (<= (* 2 a)b))

(función-<=-4 7 7 7)
(función-<=-5 -1 -2)
(función-<=-6 5 1)

; Ejercicios 3 =

(defn función-=-7
  [[a b] [s c]]
  (= [a b] [s c]))

(defn función-=-8
  [a b c]
  (= a b c))

(defn función-=-9
[a b]
  (= (inc a) b ))
 
(función-=-7 [1 2] [1 2])
(función-=-8 1 2 3)
(función-=-9 0 1)

;Ejercicios 4 >
(defn función->-8
  [a b]
  (> a b))

(defn función->-9
  [vector b]
  (> (count vector) b))


(defn función->-10
  [a b c]
  (> a b c)) 

(función->-8 1 1)
(función->-9 [1 2 3 4 1 2] 5)
(función->-10 1 2 3)


;Ejercicios 5 >=
(defn función->=12
  [a b ]
  (>= a b))

(defn función->=13
  [a b]
  (>= (* 5 a) b))

(defn función->=14
  [ a b]
  (cond (>= a b) 1 
   (>= b a) 2))

(función->=12 1 2)
(función->=13 5 25)
(función->=14 2 6)

;Ejercicios 6 assoc
(defn función-assoc-1
 [vector]
 (assoc vector 0 5))

(defn función-assoc-2
  [a, b, c]
  (assoc [a, b, c] 0 2))

(defn función-assoc-3
  [vector]
   (if (< (count vector) 2) [(assoc vector 0 true)] [(assoc vector 0 false)])
  )  

(función-assoc-1 [1 2 3])
(función-assoc-2 1 2 3)
(función-assoc-3 [1 5 6])

;Ejercicios 7 assoc-in

(defn función-assoc-in-1
  [a]
  (assoc-in {:alumno {:nombre "Fernando"}} [:alumno :nombre] a))

(defn función-assoc-in-2
  [a b]
  (assoc-in {:alumno {:nombre "Fernando", :apellido "Sanchez"}}
            [:alumno :nombre] a)
  (assoc-in {:alumno {:nombre "Fernando", :apellido "Sanchez"}}
            [:alumno :apellido] b))

(defn función-assoc-in-3
  [ a ]
  (assoc-in {:objeto {:nombre "mesa", :color "negro"}}
            [:objeto :color ] a))

(función-assoc-in-1 "Luis")
(función-assoc-in-2 "Luis" "Rodriguez")
(función-assoc-in-3 "Rojo")

;Ejercicios 8 concat
(defn función-concat-1
  [a b c d]
  (concat [a b] [c d]))

(defn función-concat-2
  [vector vector]
  (concat vector vector))

(defn función-concat-3
[a b c]
(concat {:a a :b b} {:c c} ))

(función-concat-1 1 2 3 4)
(función-concat-2 [1 2 3 4] [1 2 ])
(función-concat-3 1 87 3)

;Ejercicios 9 conj
(defn función-conj-1
  [a b c ]
  (conj [a b] c))

(defn función-conj-2
   [c]
  (conj ["a" "b"] c))

(defn función-conj-3
  [a]
  (conj #{1 2 3} a))

(función-conj-1 2 3 4)
(función-conj-2  "a")
(función-conj-3 4)

;Ejercicios Cons 
(defn función-cons-1
  [a]
  (cons a [2 4]))

(defn función-cons-2
  [vector vector]
  (cons vector vector))

(defn función-cons-3
  [a]
   ({:alumno {:nombre "Fernando"}} 
    (cons a {:alumno :nombre}))
  )

(función-cons-1 1)
(función-cons-2 [2 1 3][1])
(función-cons-3 "Luis")

;Ejercicios contains?
(defn función-contains?-1 
  [a]
  (contains? {:s 2 :b 3} a))

(defn función-contains?-2
  [a]
  (contains? [1 2 4] a))

(defn función-contains?-3
  [a]
  (contains? #{"a" "b" "s"} a ))

(función-contains?-1 :s)
(función-contains?-2 1 )
(función-contains?-3 2)

;Ejercicios Count 
(defn función-count-1
  [a]
  (count a))

(defn función-count-2
  [a b]
  (count {:uno a :dos b}))

(defn función-count-3
  [a]
  ((= (count ([1 2 3]))a)))


(función-count-1 [1 2 3])
(función-count-2 2 3)
(función-count-3 1)

;Ejercicios Disj

(defn función-disj-1
  [a]
  (disj #{1 2 3 4 5} a))

(defn función-disj-2
  [a]
  (disj a 1))

(defn función-disj-3
  [a b]
  (disj #{1 2 3 4 5 6 7 8 9 10} a b))

(función-disj-1 1)
(función-disj-2 #{1 2 3 4 5})
(función-disj-3 1 2)

;Ejercicio dissoc
(defn función-dissoc-1
  [a]
   ((dissoc a ):a))

(defn función-dissoc-2
  [a]
  (dissoc {:a 1 :b 2 :c 3 :d 4} a))

(defn función-dissoc-3
  [a b]
  (dissoc {:a 1 :b 2 :c 3 :d 4} a b))

(función-dissoc-1 {:a 1 :b 2 :c 3})

(función-dissoc-2 :a)

(función-dissoc-3 :a :b)

;Problemas Distinct

(defn función-distinct-1
  [a]
  (distinct a ))

(defn función-distinct-2
  [a]
  (distinct [1 a 2 3 4 5]))

(defn función-distinct-3
  [a b c d]
  (distinct [a b c d 4 5]))

(función-distinct-1 [1 1 2 2 3 3 4 4 5 5] )

(función-distinct-2 7)

(función-distinct-3  1 1 5 6)

;Problemas distinct?

(defn función-distinct?-1
  [a]
  (distinct? a 2 3))

(defn función-distinct?-2
  [a b]
  (distinct? a b 3))

(defn función-distinct?-3
  [a b c]
  (distinct? a b c))

(función-distinct?-1 2 )

(función-distinct?-2 1 2)

(función-distinct?-3 2 4 4)

;Problemas drop-last

(defn función-drop-last-1 
[a]
(drop-last a ) )

(defn función-drop-last-2
  [a]
  (drop-last a [1 2 3 4 5]))

(defn función-drop-last-3
  [a b c d]
  (drop-last a [b c d 5]))

(función-drop-last-1 [1 2 3 4])

(función-drop-last-2 1)
(función-drop-last-3 1 2 3 4)

;Porblemas empty
(defn función-empty-1
  [a]
  (empty a))

(defn función-empty-2
  [a b c]
  (empty [a b c]))

(defn función-empty-3
  [a]
  (empty  a))


(función-empty-1 [1 2])
(función-empty-2 3 4 5)
(función-empty-3 2)



;Problemas empty?
(defn función-empty?-1
   [a]
   (empty?  a))


(defn función-empty?-2
  [a b c d]
  (empty?  [a b c d]))

(defn función-empty?-3
  []
  (empty?  []))

(función-empty?-1 [1 2])
(función-empty?-2 1 2 3 4)
(función-empty?-3)


;Problemas even?
(defn función-even?-1
  [a]
  (even? a))

(defn función-even?-2
  [a]
  (filter even? (range a)))

(defn función-even?-3
  [a]
  (even?    (* 2 a)  )
  )

(función-even?-1 1)
(función-even?-2 7)
(función-even?-3 0)

;Problemas false?

(defn función-false?-1
 [a]
 (false? a) )


(defn función-false?-2
  [a]
  (if (false? a) [1] [5]))


(función-false?-1 true)

(función-false?-2 true)

;problemas find

(defn función-find-1
  [a]
  (find {:a 1 :b 2 :c 3 :d 4} a))

(defn función-find-2
  [a b]
  (find a b))

(defn función-find-3
  [a]
  (find [ 1 2 3 4 5] a))

(función-find-1 :a)
(función-find-2 {:a 1 :b 2 :c 3}:a)
(función-find-3 0)

;problemas con first 

(defn función-first-1
  [a b  c d]
  (first {a b c d  }))

(defn función-first-2
  [a]
  (first a))

(defn función-first-3
  [a b]
  (first [[a][b]]))


(función-first-1 "g" "g" "a" "a")
(función-first-2 [1 2 3])
(función-first-3 [1 2 3 4] [1 5 6 7])

;problemas con Flatten 

(defn función-flatten-1
  [a]
  (flatten [1 2 3 [a]]))

(defn función-flatten-2
  [a]
  (flatten a))

(defn función-flatten-3
  [a b]
  (flatten [[a][b]]))


(función-flatten-1 [4 5 6])
(función-flatten-2 [4 5 6 [12]])
(función-flatten-3 [4 5 6 [12]] [12 12])

;probleamas frequencies

(defn función-frequencies-1
  [a]
  (frequencies a))

(defn función-frequencies-2
  [a b]
  (frequencies [a b]))

(función-frequencies-1 [1 2 3 4])
(función-frequencies-2 [1 2 3 4] [123])

;Problemas get
(defn función-get-1
  [a]
  (get [1 2 3 4 5 6] a))


(defn función-get-2
  [a b]
  (get [a]b))

(defn función-get-3
  [a]
  (get {:a 2 :b 3 :c 5} a))

(función-get-1 0)
(función-get-2 [1 2 4 5 6 7] 0)
(función-get-3 :a)


(defn función-get-in-1
  [a b]
  (get-in a b))

(defn función-get-in-2
  [a b]
  (get-in a b))

(defn función-get-in-3
  [a b]
  (get-in a b))

(función-get-in-1 [[1 2 3]
                   [4 5 6]
                   [7 8 9]
                   [1 2 3]] [0 2])
(función-get-in-2  {:ursname "josue"
                    :pets [{:name "Syndra"
                            :type :dog}
                           {:name "Rayo"
                            :type :cat}]} [:pets 1 :type])
(función-get-in-3 {:a [{:b1 2} {:b2 4}] :c 3} [:a 0 :b1])

(defn función-into-1
  [a b]
  (into a b))

(defn función-into-2
  [a b]
  (into a b))

(defn función-into-3
  [a b]
  (into a b))

(función-into-1 () [[1 2 3]
                    [4 5 6]
                    [7 8 9]
                    [1 2 3]])
(función-into-2  () {:ursname "josue"
                     :pets [{:name "Syndra"
                             :type :dog}
                            {:name "Rayo"
                             :type :cat}]})
(función-into-3 [5 85 121] '(15 32 84))

(defn función-key-1
  [a]
  (key a))

(defn función-key-2
  [a]
  (key a))

(defn función-key-3
  [a]
  (key a))

(función-key-1 (last {\b :a \A :b \a :c}))
(función-key-2 (first {\g :a \H :b \h :c}))
(función-key-3 (first {\J :a \O :b \S :c}))

(defn función-keys-1
  [a]
  (keys a))

(defn función-keys-2
  [a]
  (keys a))

(defn función-keys-3
  [a]
  (keys a))

(función-keys-1 {[:a 1 :b 2 :c 3]
                 [:d 4 :e 5 :f 6]
                 [:g 7 :h 8 :i 9]
                 [:j 10 :k 11 :l 12]})
(función-keys-2  {:ursname "josue"
                  :pets [{:name "Syndra"
                          :type :dog}
                         {:name "Rayo"
                          :type :cat}]})
(función-keys-3 {:a 5 :b 85 :c 121})

(defn función-max-1
  [a b c d e]
  (max a b c d e))

(defn función-max-2
  [a]
  (max a))

(defn función-max-3
  [a b]
  (max a b))

(función-max-1 51 68 120 98 1)
(función-max-2 208)
(función-max-3 85 121)

(defn función-merge-1
  [a b]
  (merge a b))

(defn función-merge-2
  [a b]
  (merge a b))

(defn función-merge-3
  [a b c]
  (merge a b c))

(función-merge-1 {:a 1 :b 2 :c 3
                  :d 4 :e 5 :f 6
                  :g 7 :h 8}
                 {:i 10 :j 11})
(función-merge-2 {:a [51 20 88 208]} {:b [54 21 89 123]})
(función-merge-3 {:a [5 85 121]} {:b [6 95 123 84 32]} {:c [51 8964 21 54 21]})

(defn función-min-1
  [a b c]
  (min a b c))

(defn función-min-2
  [a b c d]
  (min a b c d))

(defn función-min-3
  [a b c]
  (min a b c))

(función-min-1 111 232 341)
(función-min-2 51 20 88 208)
(función-min-3 5 85 121)

(defn función-neg?-1
  [a]
  (neg? a))

(defn función-neg?-2
  [a]
  (neg? a))

(defn función-neg?-3
  [a]
  (neg? a))

(función-neg?-1 -100)
(función-neg?-2 183)
(función-neg?-3 -5)

(defn función-nil?-1
  [a]
  (nil? a))

(defn función-nil?-2
  [a]
  (nil? a))

(defn función-nil?-3
  [a]
  (map nil? a))

(función-nil?-1 nil)
(función-nil?-2 [])
(función-nil?-3 '([] {}))

(defn función-not-empty-1
  [a]
  (not-empty a))

(defn función-not-empty-2
  [a]
  (not-empty a))

(defn función-not-empty-3
  [a]
  (not-empty a))

(función-not-empty-1 [1 65 21])
(función-not-empty-2 {})
(función-not-empty-3 '())

(defn función-nth-1
  [a b]
  (nth a b))

(defn función-nth-2
  [a b]
  (nth a b))

(defn función-nth-3
  [a b]
  (nth a b))

(función-nth-1 [[15 65 32] [12 12] [15 321 89 23]] 2)
(función-nth-2 '({21 87 25 86} {1123 64 2138 72}) 0)
(función-nth-3 [132 8 123 "a" "b" "c" "d" [21 21]] 5)

(defn función-odd?-1
  [a]
  (odd? a))

(defn función-odd?-2
  [a]
  (odd? a))

(defn función-odd?-3
  [a]
  (odd? a))

(función-odd?-1 48)
(función-odd?-2 120)
(función-odd?-3 541)

(defn función-partition-1
  [a b c]
  (partition a b c))

(defn función-partition-2
  [a b c d]
  (partition a b c d))

(defn función-partition-3
  [a b c d]
  (partition a b c d))

(función-partition-1 4 [12 680] [51 8 21])
(función-partition-2 5 1 [56 02] [54 12])
(función-partition-3 3 6 ["a" "b" "c" "d" "ax" "hola"] [18 54])

(defn función-partition-all-1
  [a b]
  (partition-all a b))

(defn función-partition-all-2
  [a b c]
  (partition-all a b c))

(defn función-partition-all-3
  [a b]
  (partition-all a b))

(función-partition-all-1 4 [51 65 98 12 32 54])
(función-partition-all-2 2 4 [4 54 15 58 1 32 68 48 665 51 65 12])
(función-partition-all-3 2 "asdasdasd")

(defn función-peek-1
  [a]
  (peek a))

(defn función-peek-2
  [a]
  (peek a))

(defn función-peek-3
  [a]
  (peek a))

(función-peek-1 [51 65 98 12 32 54])
(función-peek-2 '(4 54 15 58 1 32 68 48 665 51 65 12))
(función-peek-3 [])

(defn función-pop-1
  [a]
  (pop a))

(defn función-pop-2
  [a]
  (pop a))

(defn función-pop-3
  [a]
  (pop a))

(función-pop-1 [51 65 98 12 32 54])
(función-pop-2 '(4 54 15 58 1 32 68 48 665 51 65 12))
(función-pop-3 [[52 51 68] '(51 87 32 15)])

(defn función-pos?-1
  [a]
  (pos? a))

(defn función-pos?-2
  [a]
  (pos? a))

(defn función-pos?-3
  [a]
  (pos? a))

(función-pos?-1 0)
(función-pos?-2 -156.05)
(función-pos?-3 12/54)

(defn función-quot-1
  [a b]
  (quot a b))

(defn función-quot-2
  [a b]
  (quot a b))

(defn función-quot-3
  [a b]
  (quot a b))

(función-quot-1 100 25)
(función-quot-2 -40 2)
(función-quot-3 20/2 4)

(defn función-range-1
  [a]
  (range a))

(defn función-range-2
  [a]
  (range a))

(defn función-range-3
  [a b]
  (range a b))

(función-range-1 10)
(función-range-3 10 20)
(función-range-2 20)

(defn función-rem-1
  [a b]
  (rem a b))

(defn función-rem-2
  [a b]
  (rem a b))

(defn función-rem-3
  [a b]
  (rem a b))

(función-rem-1 100 30)
(función-rem-2 -40 3)
(función-rem-3 20/2 4)

(defn función-repeat-1
  [a b]
  (repeat a b))

(defn función-repeat-2
  [a b]
  (repeat a b))

(defn función-repeat-3
  [a b]
  (repeat a b))

(función-repeat-1 2 "x")
(función-repeat-2 5 "hola")
(función-repeat-3 5 [21 84 68])

(defn función-replace-1
  [a b]
  (replace a b))

(defn función-replace-2
  [a b]
  (replace a b))

(defn función-replace-3
  [a b]
  (replace a b))

(función-replace-1 [0 2 58 51 152 32] [0 5 8])
(función-replace-2 {:a 51 :b 1} {:c 5 :d 8})
(función-replace-3 [51 8] [54 84])

(defn función-rest-1
  [a]
  (rest a))

(defn función-rest-2
  [a]
  (rest a))

(defn función-rest-3
  [a]
  (rest a))

(función-rest-1 [51 68 12 35])
(función-rest-2 [[-51 -20] [65 89]])
(función-rest-3 '(85 121 -5641 -56 -15 -5))

(defn función-select-keys-1
  [a b]
  (select-keys a b))

(defn función-select-keys-2
  [a b]
  (select-keys a b))

(defn función-select-keys-3
  [a b]
  (select-keys a b))

(función-select-keys-1 {:a 51 :b 68 :c 12 :d 35} [:c])
(función-select-keys-2 [:A [-51 -20] :B [65 89]] [2 1])
(función-select-keys-3 [:a 85 :b 121 :c 5641 :d -56 :f -15 :g -5] [3 4])

(defn función-shuffle-1
  [a]
  (shuffle a))

(defn función-shuffle-2
  [a]
  (shuffle a))

(defn función-shuffle-3
  [a]
  (shuffle a))

(función-shuffle-1 [51 38 65 120])
(función-shuffle-2 '([-51 -20] [65 89] [554 89]))
(función-shuffle-3 '(1 2 3 4 5 6))

(defn función-sort-1
  [a]
  (sort a))

(defn función-sort-2
  [a]
  (sort a))

(defn función-sort-3
  [a]
  (sort a))

(función-sort-1 [51 38 65 120])
(función-sort-2 '([-51 -20] [65 89] [554 89]))
(función-sort-3 '(1 2 3 4 5 6))

(defn función-split-at-1
  [a b]
  (split-at b a))

(defn función-split-at-2
  [a b]
  (split-at b a))

(defn función-split-at-3
  [a b]
  (split-at b a))

(función-split-at-1 [51 38 65 120] 3)
(función-split-at-2 '([-51 -20] [65 89] [554 89]) 2)
(función-split-at-3 '(1 2 3 4 5 6) 5)

(defn función-str-1
  [a]
  (str a))

(defn función-str-2
  [a]
  (str a))

(defn función-str-3
  [a]
  (str a))

(función-str-1 [[51 38 65 120] [21 98 13]])
(función-str-2 '("Mi perro" "es" "azul"))
(función-str-3 '(:a :b :c :d :e))

(defn función-subs-1
  [a b c]
  (subs a b c))

(defn función-subs-2
  [a b]
  (subs a b))

(defn función-subs-3
  [a b]
  (subs a b))

(función-subs-1 "Mi perro es azul" 4 8)
(función-subs-2  "Hola como estas" 2)
(función-subs-3 "1 2 3 4 5 6 7 8 9 10" 2)

(defn función-subvec-1
  [a b]
  (subvec a b))

(defn función-subvec-2
  [a b c]
  (subvec a b c))

(defn función-subvec-3
  [a b]
  (subvec a b))

(función-subvec-1 [51 89 32 12] 2)
(función-subvec-2  [12 32 65 78 41 63] 2 5)
(función-subvec-3 [[32 98] [153 2] [12 15]] 2)


(defn función-take-1
  [a b]
  (take a b))

(defn función-take-2
  [a b]
  (take b a))

(defn función-take-3
  [a b]
  (take a b))

(función-take-1 3 [51 89 32 12])
(función-take-2  '(12 32 65 78) 3)
(función-take-3 2 [[32 98] [153 2] [12 15]])

(defn función-true?-1
  [a]
  (true? a))

(defn función-true?-2
  [a]
  (true? a))

(defn función-true?-3
  [a]
  (true? a))

(función-true?-1 -100)
(función-true?-2 [true -20 true 208])
(función-true?-3 (= 1 1))

(defn función-val-1
  [a]
  (val a))

(defn función-val-2
  [a]
  (val a))

(defn función-val-3
  [a]
  (val a))

(función-val-1 (last {:a 15 :b 12 :c 18 :d 88}))
(función-val-2 (first {:a [15 98 12] :b [12 1] :c [98 56]}))
(función-val-3 (last {:a [321 1] :b [12 15] :c [51 87]}))

(defn función-vals-1
  [a]
  (vals a))

(defn función-vals-2
  [a]
  (vals a))

(defn función-vals-3
  [a]
  (vals a))

(función-vals-1 {:a 15 :b 12 :c 18 :d 88})
(función-vals-2 {:a {15 98} :b {12 1} :c {98 56}})
(función-vals-3 {:a [321 1] :b [12 15] :c [51 87]})

(defn función-zero?-1
  [a]
  (zero? a))

(defn función-zero?-2
  [a]
  (zero? a))

(defn función-zero?-3
  [a]
  (zero? a))

(función-zero?-1 0.0)
(función-zero?-2 0.1)
(función-zero?-3 54)

(defn función-zipmap-1
  [a b]
  (zipmap a b))

(defn función-zipmap-2
  [a b]
  (zipmap a b))

(defn función-zipmap-3
  [a b]
  (zipmap a b))

(función-zipmap-1 [51 98] [:a :b])
(función-zipmap-2 ["asd" "hola" "bye"] ["hola" "como" "estas"])
(función-zipmap-3 ["hola" "bye"] [:a :b :c])(función-get-in-1 [[1 2 3]
                                                               [4 5 6]
                                                               [7 8 9]
                                                               [1 2 3]] [0 2])
(función-get-in-2  {:ursname "josue"
                    :pets [{:name "Syndra"
                            :type :dog}
                           {:name "Rayo"
                            :type :cat}]} [:pets 1 :type])
(función-get-in-3 {:a [{:b1 2} {:b2 4}] :c 3} [:a 0 :b1])

(defn función-into-1
  [a b]
  (into a b))

(defn función-into-2
  [a b]
  (into a b))

(defn función-into-3
  [a b]
  (into a b))

(función-into-1 () [[1 2 3]
                    [4 5 6]
                    [7 8 9]
                    [1 2 3]])
(función-into-2  () {:ursname "josue"
                     :pets [{:name "Syndra"
                             :type :dog}
                            {:name "Rayo"
                             :type :cat}]})
(función-into-3 [5 85 121] '(15 32 84))

(defn función-key-1
  [a]
  (key a))

(defn función-key-2
  [a]
  (key a))

(defn función-key-3
  [a]
  (key a))

(función-key-1 (last {\b :a \A :b \a :c}))
(función-key-2 (first {\g :a \H :b \h :c}))
(función-key-3 (first {\J :a \O :b \S :c}))

(defn función-keys-1
  [a]
  (keys a))

(defn función-keys-2
  [a]
  (keys a))

(defn función-keys-3
  [a]
  (keys a))

(función-keys-1 {[:a 1 :b 2 :c 3]
                 [:d 4 :e 5 :f 6]
                 [:g 7 :h 8 :i 9]
                 [:j 10 :k 11 :l 12]})
(función-keys-2  {:ursname "josue"
                  :pets [{:name "Syndra"
                          :type :dog}
                         {:name "Rayo"
                          :type :cat}]})
(función-keys-3 {:a 5 :b 85 :c 121})

(defn función-max-1
  [a b c d e]
  (max a b c d e))

(defn función-max-2
  [a]
  (max a))

(defn función-max-3
  [a b]
  (max a b))

(función-max-1 51 68 120 98 1)
(función-max-2 208)
(función-max-3 85 121)

(defn función-merge-1
  [a b]
  (merge a b))

(defn función-merge-2
  [a b]
  (merge a b))

(defn función-merge-3
  [a b c]
  (merge a b c))

(función-merge-1 {:a 1 :b 2 :c 3
                  :d 4 :e 5 :f 6
                  :g 7 :h 8}
                 {:i 10 :j 11})
(función-merge-2 {:a [51 20 88 208]} {:b [54 21 89 123]})
(función-merge-3 {:a [5 85 121]} {:b [6 95 123 84 32]} {:c [51 8964 21 54 21]})

(defn función-min-1
  [a b c]
  (min a b c))

(defn función-min-2
  [a b c d]
  (min a b c d))

(defn función-min-3
  [a b c]
  (min a b c))

(función-min-1 111 232 341)
(función-min-2 51 20 88 208)
(función-min-3 5 85 121)

(defn función-neg?-1
  [a]
  (neg? a))

(defn función-neg?-2
  [a]
  (neg? a))

(defn función-neg?-3
  [a]
  (neg? a))

(función-neg?-1 -100)
(función-neg?-2 183)
(función-neg?-3 -5)

(defn función-nil?-1
  [a]
  (nil? a))

(defn función-nil?-2
  [a]
  (nil? a))

(defn función-nil?-3
  [a]
  (map nil? a))

(función-nil?-1 nil)
(función-nil?-2 [])
(función-nil?-3 '([] {}))

(defn función-not-empty-1
  [a]
  (not-empty a))

(defn función-not-empty-2
  [a]
  (not-empty a))

(defn función-not-empty-3
  [a]
  (not-empty a))

(función-not-empty-1 [1 65 21])
(función-not-empty-2 {})
(función-not-empty-3 '())

(defn función-nth-1
  [a b]
  (nth a b))

(defn función-nth-2
  [a b]
  (nth a b))

(defn función-nth-3
  [a b]
  (nth a b))

(función-nth-1 [[15 65 32] [12 12] [15 321 89 23]] 2)
(función-nth-2 '({21 87 25 86} {1123 64 2138 72}) 0)
(función-nth-3 [132 8 123 "a" "b" "c" "d" [21 21]] 5)

(defn función-odd?-1
  [a]
  (odd? a))

(defn función-odd?-2
  [a]
  (odd? a))

(defn función-odd?-3
  [a]
  (odd? a))

(función-odd?-1 48)
(función-odd?-2 120)
(función-odd?-3 541)

(defn función-partition-1
  [a b c]
  (partition a b c))

(defn función-partition-2
  [a b c d]
  (partition a b c d))

(defn función-partition-3
  [a b c d]
  (partition a b c d))

(función-partition-1 4 [12 680] [51 8 21])
(función-partition-2 5 1 [56 02] [54 12])
(función-partition-3 3 6 ["a" "b" "c" "d" "ax" "hola"] [18 54])

(defn función-partition-all-1
  [a b]
  (partition-all a b))

(defn función-partition-all-2
  [a b c]
  (partition-all a b c))

(defn función-partition-all-3
  [a b]
  (partition-all a b))

(función-partition-all-1 4 [51 65 98 12 32 54])
(función-partition-all-2 2 4 [4 54 15 58 1 32 68 48 665 51 65 12])
(función-partition-all-3 2 "asdasdasd")

(defn función-peek-1
  [a]
  (peek a))

(defn función-peek-2
  [a]
  (peek a))

(defn función-peek-3
  [a]
  (peek a))

(función-peek-1 [51 65 98 12 32 54])
(función-peek-2 '(4 54 15 58 1 32 68 48 665 51 65 12))
(función-peek-3 [])

(defn función-pop-1
  [a]
  (pop a))

(defn función-pop-2
  [a]
  (pop a))

(defn función-pop-3
  [a]
  (pop a))

(función-pop-1 [51 65 98 12 32 54])
(función-pop-2 '(4 54 15 58 1 32 68 48 665 51 65 12))
(función-pop-3 [[52 51 68] '(51 87 32 15)])

(defn función-pos?-1
  [a]
  (pos? a))

(defn función-pos?-2
  [a]
  (pos? a))

(defn función-pos?-3
  [a]
  (pos? a))

(función-pos?-1 0)
(función-pos?-2 -156.05)
(función-pos?-3 12/54)

(defn función-quot-1
  [a b]
  (quot a b))

(defn función-quot-2
  [a b]
  (quot a b))

(defn función-quot-3
  [a b]
  (quot a b))

(función-quot-1 100 25)
(función-quot-2 -40 2)
(función-quot-3 20/2 4)

(defn función-range-1
  [a]
  (range a))

(defn función-range-2
  [a]
  (range a))

(defn función-range-3
  [a b]
  (range a b))

(función-range-1 10)
(función-range-3 10 20)
(función-range-2 20)

(defn función-rem-1
  [a b]
  (rem a b))

(defn función-rem-2
  [a b]
  (rem a b))

(defn función-rem-3
  [a b]
  (rem a b))

(función-rem-1 100 30)
(función-rem-2 -40 3)
(función-rem-3 20/2 4)

(defn función-repeat-1
  [a b]
  (repeat a b))

(defn función-repeat-2
  [a b]
  (repeat a b))

(defn función-repeat-3
  [a b]
  (repeat a b))

(función-repeat-1 2 "x")
(función-repeat-2 5 "hola")
(función-repeat-3 5 [21 84 68])

(defn función-replace-1
  [a b]
  (replace a b))

(defn función-replace-2
  [a b]
  (replace a b))

(defn función-replace-3
  [a b]
  (replace a b))

(función-replace-1 [0 2 58 51 152 32] [0 5 8])
(función-replace-2 {:a 51 :b 1} {:c 5 :d 8})
(función-replace-3 [51 8] [54 84])

(defn función-rest-1
  [a]
  (rest a))

(defn función-rest-2
  [a]
  (rest a))

(defn función-rest-3
  [a]
  (rest a))

(función-rest-1 [51 68 12 35])
(función-rest-2 [[-51 -20] [65 89]])
(función-rest-3 '(85 121 -5641 -56 -15 -5))

(defn función-select-keys-1
  [a b]
  (select-keys a b))

(defn función-select-keys-2
  [a b]
  (select-keys a b))

(defn función-select-keys-3
  [a b]
  (select-keys a b))

(función-select-keys-1 {:a 51 :b 68 :c 12 :d 35} [:c])
(función-select-keys-2 [:A [-51 -20] :B [65 89]] [2 1])
(función-select-keys-3 [:a 85 :b 121 :c 5641 :d -56 :f -15 :g -5] [3 4])

(defn función-shuffle-1
  [a]
  (shuffle a))

(defn función-shuffle-2
  [a]
  (shuffle a))

(defn función-shuffle-3
  [a]
  (shuffle a))

(función-shuffle-1 [51 38 65 120])
(función-shuffle-2 '([-51 -20] [65 89] [554 89]))
(función-shuffle-3 '(1 2 3 4 5 6))

(defn función-sort-1
  [a]
  (sort a))

(defn función-sort-2
  [a]
  (sort a))

(defn función-sort-3
  [a]
  (sort a))

(función-sort-1 [51 38 65 120])
(función-sort-2 '([-51 -20] [65 89] [554 89]))
(función-sort-3 '(1 2 3 4 5 6))

(defn función-split-at-1
  [a b]
  (split-at b a))

(defn función-split-at-2
  [a b]
  (split-at b a))

(defn función-split-at-3
  [a b]
  (split-at b a))

(función-split-at-1 [51 38 65 120] 3)
(función-split-at-2 '([-51 -20] [65 89] [554 89]) 2)
(función-split-at-3 '(1 2 3 4 5 6) 5)

(defn función-str-1
  [a]
  (str a))

(defn función-str-2
  [a]
  (str a))

(defn función-str-3
  [a]
  (str a))

(función-str-1 [[51 38 65 120] [21 98 13]])
(función-str-2 '("Mi perro" "es" "azul"))
(función-str-3 '(:a :b :c :d :e))

(defn función-subs-1
  [a b c]
  (subs a b c))

(defn función-subs-2
  [a b]
  (subs a b))

(defn función-subs-3
  [a b]
  (subs a b))

(función-subs-1 "Mi perro es azul" 4 8)
(función-subs-2  "Hola como estas" 2)
(función-subs-3 "1 2 3 4 5 6 7 8 9 10" 2)

(defn función-subvec-1
  [a b]
  (subvec a b))

(defn función-subvec-2
  [a b c]
  (subvec a b c))

(defn función-subvec-3
  [a b]
  (subvec a b))

(función-subvec-1 [51 89 32 12] 2)
(función-subvec-2  [12 32 65 78 41 63] 2 5)
(función-subvec-3 [[32 98] [153 2] [12 15]] 2)


(defn función-take-1
  [a b]
  (take a b))

(defn función-take-2
  [a b]
  (take b a))

(defn función-take-3
  [a b]
  (take a b))

(función-take-1 3 [51 89 32 12])
(función-take-2  '(12 32 65 78) 3)
(función-take-3 2 [[32 98] [153 2] [12 15]])

(defn función-true?-1
  [a]
  (true? a))

(defn función-true?-2
  [a]
  (true? a))

(defn función-true?-3
  [a]
  (true? a))

(función-true?-1 -100)
(función-true?-2 [true -20 true 208])
(función-true?-3 (= 1 1))

(defn función-val-1
  [a]
  (val a))

(defn función-val-2
  [a]
  (val a))

(defn función-val-3
  [a]
  (val a))

(función-val-1 (last {:a 15 :b 12 :c 18 :d 88}))
(función-val-2 (first {:a [15 98 12] :b [12 1] :c [98 56]}))
(función-val-3 (last {:a [321 1] :b [12 15] :c [51 87]}))

(defn función-vals-1
  [a]
  (vals a))

(defn función-vals-2
  [a]
  (vals a))

(defn función-vals-3
  [a]
  (vals a))

(función-vals-1 {:a 15 :b 12 :c 18 :d 88})
(función-vals-2 {:a {15 98} :b {12 1} :c {98 56}})
(función-vals-3 {:a [321 1] :b [12 15] :c [51 87]})

(defn función-zero?-1
  [a]
  (zero? a))

(defn función-zero?-2
  [a]
  (zero? a))

(defn función-zero?-3
  [a]
  (zero? a))

(función-zero?-1 0.0)
(función-zero?-2 0.1)
(función-zero?-3 54)

(defn función-zipmap-1
  [a b]
  (zipmap a b))

(defn función-zipmap-2
  [a b]
  (zipmap a b))

(defn función-zipmap-3
  [a b]
  (zipmap a b))

(función-zipmap-1 [51 98] [:a :b])
(función-zipmap-2 ["asd" "hola" "bye"] ["hola" "como" "estas"])
(función-zipmap-3 ["hola" "bye"] [:a :b :c])